package com.example.ej.ziphiotest;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.example.ej.ziphiotest.model.RestResponse;
import com.example.ej.ziphiotest.requests.UserRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class DashboardActivity extends AppCompatActivity {

    private String apiKey = "";
    private UserRequest request;
    private JSONArray usersArray;
    private ArrayList<HashMap<String, String>> userModelArrayList;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        setTitle("USERS LIST - ZIPHIO");
        initEnvironment();
        sendUserRequest();
    }

    private void initEnvironment() {
        getAPIkey();
        userModelArrayList = new ArrayList<HashMap<String, String>>();
        listView = (ListView) findViewById(R.id.listView);
    }

    private void sendUserRequest() {
        if (apiKey.length() > 0) {
            request = new UserRequest(new OnTaskComplete() {
                @Override
                public void onTaskComplete(RestResponse response) {
                    usersArray = response.getUsers();

                    for (int i = 0; i <= usersArray.length(); i++) {
                        try {
                            JSONObject object = (JSONObject) usersArray.get(i);
                            HashMap<String, String> hashMap = new HashMap<String, String>();

                            hashMap.put("name", object.getString("name"));
                            hashMap.put("email", object.getString("email"));
                            userModelArrayList.add(hashMap);

                            final ListAdapter adapter = new SimpleAdapter(
                                    DashboardActivity.this,
                                    userModelArrayList,
                                    R.layout.user_list,
                                    new String[]{"name", "email"},
                                    new int[]{R.id.tvName, R.id.tvEmail});

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    listView.setAdapter(adapter);
                                }
                            });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            request.execute(apiKey);
        }
    }

    private void getAPIkey() {
        Intent intent = getIntent();
        this.apiKey = intent.getStringExtra("api_key");
    }

}
