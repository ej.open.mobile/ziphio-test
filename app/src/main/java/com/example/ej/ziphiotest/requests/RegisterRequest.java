package com.example.ej.ziphiotest.requests;


import android.os.AsyncTask;

import com.example.ej.ziphiotest.OnTaskComplete;
import com.example.ej.ziphiotest.ZiphioUtil;
import com.example.ej.ziphiotest.model.RestResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class RegisterRequest extends AsyncTask<String, Void, String> {

    private OkHttpClient client = new OkHttpClient();
    private String URI = ZiphioUtil.BASE_URL + "/register";
    private RestResponse restResponse;
    private OnTaskComplete onTaskComplete;
    private JSONObject jsonObject;

    public RegisterRequest(OnTaskComplete taskComplete){
        this.onTaskComplete = taskComplete;

    }

    @Override
    protected String doInBackground(String... params) {

        if (params == null){
            onTaskComplete.onTaskComplete(new RestResponse("error", null, "params are empty", null, null));
            return null;
        }

        RequestBody formBody = new FormBody.Builder()
                .add("name", params[0])
                .add("email", params[1])
                .add("password", params[2])
                .build();

        Request request = new Request.Builder()
                .url(URI)
                .post(formBody)
                .build();

        try {
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    call.cancel();
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (response.code() == 200){
                        try {
                            jsonObject = new JSONObject(response.body().string());
                            if (jsonObject.getString("status").equals("success")){
                                restResponse = new RestResponse(getTxt("status"), getTxt("code"), getTxt("message"), null, null);
                                onTaskComplete.onTaskComplete(restResponse);
                            }else {
                                onTaskComplete.onTaskComplete(new RestResponse(getTxt("status"), null, getTxt("message"), null, null));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }else {
                        onTaskComplete.onTaskComplete(new RestResponse("error", null, "not connected with the rest", null, null));
                    }

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private String getTxt(String s){
        if (s != null){
            try {
                return jsonObject.getString(s);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

}
