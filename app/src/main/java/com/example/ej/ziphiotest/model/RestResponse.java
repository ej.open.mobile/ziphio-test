package com.example.ej.ziphiotest.model;


import org.json.JSONArray;

import java.lang.reflect.Array;
import java.util.Arrays;

public class RestResponse {

    private String status;
    private String code;
    private String message;
    private String key;
    private JSONArray users;

    public RestResponse(String status, String code, String message, String key, JSONArray users) {
        this.status = status;
        this.code = code;
        this.message = message;
        this.key = key;
        this.users = users;

    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public JSONArray getUsers() {
        return users;
    }

    public void setUsers(JSONArray users) {
        this.users = users;
    }

    @Override
    public String toString() {
        return "RestResponse{" +
                "status='" + status + '\'' +
                ", code='" + code + '\'' +
                ", message='" + message + '\'' +
                ", key='" + key + '\'' +
                ", users=" + users +
                '}';
    }
}
