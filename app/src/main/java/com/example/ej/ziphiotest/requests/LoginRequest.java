package com.example.ej.ziphiotest.requests;

import android.os.AsyncTask;
import android.util.Log;

import com.example.ej.ziphiotest.OnTaskComplete;
import com.example.ej.ziphiotest.ZiphioUtil;
import com.example.ej.ziphiotest.model.RestResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.sql.Time;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class LoginRequest extends AsyncTask <String, Void, String> {

    private OkHttpClient client = new OkHttpClient();
    private String URL = ZiphioUtil.BASE_URL + "/login";
    private RestResponse restResponse;
    private OnTaskComplete onTaskComplete;
    private JSONObject jsonObject;

    public LoginRequest(OnTaskComplete taskComplete){
        this.onTaskComplete = taskComplete;

    }

    @Override
    protected String doInBackground(String... params) {
        client.newBuilder().connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();

        if (params == null){
            onTaskComplete.onTaskComplete(new RestResponse("error", null, "params are empty", null, null));
            return null;
        }

        RequestBody formBody = new FormBody.Builder()
                .add("email", params[0])
                .add("password", params[1])
                .build();

        Request request = new Request.Builder()
                .url(URL)
                .post(formBody)
                .build();

        try {
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    call.cancel();
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (response.code() == 200){
                        try {
                            jsonObject = new JSONObject(response.body().string());
                            Log.e("aaa", "status   " + jsonObject.getString("status"));
                            if (jsonObject.getString("status").equals("success")){
                                restResponse = new RestResponse(jsonObject.getString("status"), jsonObject.getString("code"), jsonObject.getString("message"), jsonObject.getString("key"), null);
                                onTaskComplete.onTaskComplete(restResponse);
                            }else {
                                onTaskComplete.onTaskComplete(new RestResponse(jsonObject.getString("status"), null, jsonObject.getString("message"), null, null));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }else {
                        onTaskComplete.onTaskComplete(new RestResponse("error", null, "check connection with rest service", null, null));
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
