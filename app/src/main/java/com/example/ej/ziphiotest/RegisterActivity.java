package com.example.ej.ziphiotest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.ej.ziphiotest.model.RestResponse;
import com.example.ej.ziphiotest.requests.RegisterRequest;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener, OnTaskComplete{

    private EditText userName;
    private EditText email;
    private EditText password;
    private EditText verifyPassword;
    private Button submit;
    private Button signIn;

    RegisterRequest request;
    ZiphioUtil util;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        initEnvironment();

    }
    /**
     * init context
     * */
    private void initEnvironment() {
        userName = (EditText) findViewById(R.id.etName);
        email = (EditText) findViewById(R.id.etEmail);
        password = (EditText) findViewById(R.id.etPass);
        verifyPassword = (EditText) findViewById(R.id.etVerifyPassword);
        submit = (Button) findViewById(R.id.btnUserRegister);
        signIn = (Button) findViewById(R.id.btnSigninFromRegister);
        util = new ZiphioUtil(this);

        submit.setOnClickListener(this);
        signIn.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnUserRegister: {
                if (validateInputs()){
                    request = new RegisterRequest(RegisterActivity.this);
                    request.execute(returnText(userName), returnText(email), returnText(password));
                }
                 break;
            }
            case R.id.btnSigninFromRegister: {
                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                break;
            }
        }
    }

    private String returnText(EditText editText){
        return editText.getText().toString();
    }

    /**
     * validate user inputs
     * */
    private boolean validateInputs() {
        if (!util.isConnected()){
            Toast.makeText(this, "Check Network Connection", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!checkLenth(userName)){
            userName.setError("Enter Name");
            return false;
        }else {
            if (!checkLenth(password)){
                password.setError("Enter password");
                return false;
            }else {
                if (!checkLenth(email)){
                    email.setError("Enter email");
                    return false;
                }else {
                    if (!checkLenth(verifyPassword)){
                        verifyPassword.setError("Enter password again");
                        return false;
                    }else {
                        if (!password.getText().toString().equals(verifyPassword.getText().toString())){
                            password.setError("verification failed");
                            verifyPassword.setError("verification failed");
                            return false;
                        }else {
                            if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()){
                                email.setError("Please enter valid email");
                                return false;
                            }else {
                                return true;
                            }
                        }
                    }
                }
            }
        }
    }

    private boolean checkLenth(EditText editText){
        if (editText.getText().toString().length() > 0){
            return true;
        }
        return false;
    }

    /**
     * Will call after the async task executed
     * */

    @Override
    public void onTaskComplete(final RestResponse s) {
        if (s != null){

            if (s.getStatus().equals("success") && s.getCode().equals("1")){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "You've bees successfully registered!", Toast.LENGTH_SHORT).show();
                    }
                });
                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
            }else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), s.getStatus() + " : " + s.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
        }else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(), "response didn't received", Toast.LENGTH_SHORT).show();
                }
            });
        }

    }
}
