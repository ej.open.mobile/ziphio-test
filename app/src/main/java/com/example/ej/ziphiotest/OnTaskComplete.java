package com.example.ej.ziphiotest;

import com.example.ej.ziphiotest.model.RestResponse;

/**
 * interface for activate when async task completed
 * */
public interface OnTaskComplete {
    void onTaskComplete(RestResponse response);
}
