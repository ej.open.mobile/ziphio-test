package com.example.ej.ziphiotest.requests;

import android.os.AsyncTask;

import com.example.ej.ziphiotest.OnTaskComplete;
import com.example.ej.ziphiotest.ZiphioUtil;
import com.example.ej.ziphiotest.model.RestResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class UserRequest extends AsyncTask<String, Void, String> {

    private OkHttpClient client = new OkHttpClient();
    private String URL = ZiphioUtil.BASE_URL + "/users";
    private RestResponse restResponse;
    private OnTaskComplete onTaskComplete;
    private JSONObject jsonObject;

    public UserRequest(OnTaskComplete taskComplete) {
        this.onTaskComplete = taskComplete;
    }

    @Override
    protected String doInBackground(String... params) {
        client.newBuilder().connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();

        if (params == null) {
            onTaskComplete.onTaskComplete(new RestResponse("error", null, "params are empty", null, null));
            return null;
        }

        final Request request = new Request.Builder()
                .header("key", params[0])
                .url(URL)
                .build();

        try {
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    call.cancel();
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {

                    //Log.e("aaa","response ::: " + response.body().string());

                    if (response.code() == 200){
                        try {
                            jsonObject = new JSONObject(response.body().string());
                            if (jsonObject.getString("status").equals("success")){
                                restResponse = new RestResponse(jsonObject.getString("status"), jsonObject.getString("code"), jsonObject.getString("message"), null, jsonObject.getJSONArray("users"));
                                onTaskComplete.onTaskComplete(restResponse);
                            }else {
                                onTaskComplete.onTaskComplete(new RestResponse(jsonObject.getString("status"), null, jsonObject.getString("message"), null, null));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }else {
                        onTaskComplete.onTaskComplete(new RestResponse("error", null, "connection problem with rest service", null, null));
                    }

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
