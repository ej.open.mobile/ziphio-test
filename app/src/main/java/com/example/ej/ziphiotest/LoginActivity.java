package com.example.ej.ziphiotest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ej.ziphiotest.model.RestResponse;
import com.example.ej.ziphiotest.requests.LoginRequest;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, OnTaskComplete{

    private EditText username;
    private EditText password;
    private Button login;
    private TextView newAccount;

    private LoginRequest request;
    private ZiphioUtil util;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initEnvironment();

    }

    /**
     * Init the context
     * */
    private void initEnvironment() {
        username =  (EditText) findViewById(R.id.etUsername);
        password =  (EditText) findViewById(R.id.etPassword);
        login = (Button) findViewById(R.id.btnLogin);
        newAccount = (TextView) findViewById(R.id.txtNewAccount);
        util = new ZiphioUtil(this);

        login.setOnClickListener(this);
        newAccount.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnLogin: {

                if (!util.isConnected()){
                    Toast.makeText(this, "Check Network Connection", Toast.LENGTH_SHORT).show();
                    break;
                }
                if (!checkLength(username)){
                    username.setError("Please enter email");
                }else {
                    if (!checkLength(password)){
                        password.setError("Please enter password");
                    }else {
                        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(username.getText().toString()).matches()){
                            username.setError("Please enter valid email");
                        }else {
                            request = new LoginRequest(this);
                            request.execute(username.getText().toString(), password.getText().toString());
                            login.setEnabled(false);
                        }

                    }
                }
                break;
            }
            case R.id.txtNewAccount: {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
                break;
            }
        }
    }

    private boolean checkLength(EditText editText){
        if (editText.getText().toString().length() >0){
            return true;
        }
        return false;
    }

    @Override
    public void onTaskComplete(final RestResponse response) {
        if (response != null){
            if (response.getStatus().equals("success") && response.getCode().equals("1")){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), response.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
                Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);
                intent.putExtra("api_key", response.getKey());
                startActivity(intent);
            }else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        login.setEnabled(true);
                        username.setError("check again");
                        password.setError("check again");
                        Toast.makeText(getApplicationContext(), "status: " + response.getStatus() + " and message is: " + response.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }

        }else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    login.setEnabled(true);
                    Toast.makeText(LoginActivity.this, "Response didn't received", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        login.setEnabled(true);
    }
}
