package com.example.ej.ziphiotest;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class ZiphioUtil {

    private static Context context;

    public ZiphioUtil(Context context) {
        this.context = context;
    }

    public static final String BASE_URL= "http://107.191.60.175:8778";

    public boolean isConnected(){
        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

}
